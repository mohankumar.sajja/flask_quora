FROM python:3.6-alpine

RUN adduser -D flaskq

WORKDIR /home/flaskq

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn
RUN venv/bin/pip install gunicorn pymysql

COPY app app
COPY migrations migrations
COPY flaskq.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP flaskq.py

RUN chown -R flaskq:flaskq ./
USER flaskq

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]