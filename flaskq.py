from app import create_app, db
from app.models import User, Question, Answer, Follow, Vote

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Question': Question, 'Answer': Answer, 'Follow': Follow, 'Vote':Vote}