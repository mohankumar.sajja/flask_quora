#!/usr/bin/env python
import os
from app import celery, create_app
from config import Config
 
app = create_app()
app.app_context().push()