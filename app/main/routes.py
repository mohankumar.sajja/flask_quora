from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app, abort
from flask_login import current_user, login_required
from app import db
from app.main.forms import EditProfileForm, QuestionForm, AnswerForm
from app.models import User, Question, Answer, Follow, Vote
from app.main import bp
from app.main.forms import SearchForm
import os
from app import celery
from app.main.tasks import create_log

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()

@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    form = QuestionForm()
    if form.validate_on_submit():
        question = Question(body=form.body.data,
                            detail=form.detail.data,
                            author=current_user)
        db.session.add(question)
        db.session.commit()
        flash('Your Question posted successfully!')
        return redirect(url_for('main.index'))
    questions = Question.query.order_by(Question.timestamp.desc()).all()
    return render_template('index.html', title='Home',form=form, questions=questions)

@bp.route('/questions/<int:id>')
@login_required
def questions(id):
    question = Question.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = question.answers.paginate(
            page, per_page=current_app.config['POSTS_PER_PAGE'],
            error_out=False)
    answers = pagination.items
    return render_template('question.html', questions=[question],
                           answers=answers, pagination=pagination)

@bp.route('/answers/<int:id>', methods=['GET', 'POST'])
@login_required
def answers(id):
    question = Question.query.get_or_404(id)
    form = AnswerForm()
    if form.validate_on_submit():
        answer = Answer(body=form.body.data,
                        question=question,
                        author=current_user._get_current_object())
        db.session.add(answer)
        db.session.commit()
        flash('Your answer has been published.')
        return redirect(url_for('.questions', id=question.id))
    return render_template('edit_answer.html', form=form, id=question.id,
                           new=True)

@bp.route('/edit-question/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_question(id):
    question = Question.query.get_or_404(id)
    if current_user != question.author:
        abort(403)
    form = QuestionForm()
    if form.validate_on_submit():
        question.body = form.body.data
        question.detail = form.detail.data
        db.session.add(question)
        db.session.commit()
        flash('The question has been updated.')
        return redirect(url_for('.questions', id=question.id))
    form.body.data = question.body
    form.detail.data = question.detail
    return render_template('edit_question.html', form=form)

@bp.route('/edit-answer/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_answer(id):
    answer = Answer.query.get_or_404(id)
    if current_user != answer.author:
        abort(403)
    form = AnswerForm()
    if form.validate_on_submit():
        answer.body = form.body.data
        db.session.add(answer)
        db.session.commit()
        flash('The answer has been updated.')
        return redirect(url_for('.questions', id=answer.question_id))
    form.body.data = answer.body
    return render_template('edit_answer.html', form=form, id=id)

@bp.route('/users/<username>')
@login_required
def users(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    questions = user.questions.order_by(Question.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.user', username=user.username,
                       page=questions.next_num) if questions.has_next else None
    prev_url = url_for('main.user', username=user.username,
                       page=questions.prev_num) if questions.has_prev else None
    return render_template('user.html', user=user, questions=questions.items,
                           next_url=next_url, prev_url=prev_url)

@bp.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user)
        db.session.commit()
        flash('Your profile has been updated.')
        return redirect(url_for('main.users', username=current_user.username))
    elif request.method == 'GET':
        form.name.data = current_user.name
        form.location.data = current_user.location
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title="Edit Profile", form=form)

@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    if current_user.is_following(user):
        flash('You are already following this user.')
        return redirect(url_for('.users', username=username))
    current_user.follow(user)
    flash('You are now following %s.' % username)
    return redirect(url_for('.users', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    if not current_user.is_following(user):
        flash('You are not following this user.')
        return redirect(url_for('.users', username=username))
    current_user.unfollow(user)
    flash('You are not following %s anymore.' % username)
    return redirect(url_for('.users', username=username))



@bp.route('/vote/<int:id>')
@login_required
def vote(id):
    answer = Answer.query.get_or_404(id)
    type = request.args.get('type')
    current_user.vote(answer, type)
    db.session.add(current_user)
    db.session.commit()
    return jsonify(url=url_for('.unvote', id=id), upvotes=answer.upvotes)


@bp.route('/unvote/<int:id>')
@login_required
def unvote(id):
    answer = Answer.query.get_or_404(id)
    type = request.args.get('type')
    current_user.unvote(answer, type)
    db.session.add(current_user)
    db.session.commit()
    return jsonify(url=url_for('.vote', id=id), upvotes=answer.upvotes)


@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)
    questions, total = Question.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title='Search', questions=questions,
                           next_url=next_url, prev_url=prev_url)

# #CELERY

# #celery implementation
@bp.route('/generate_file/<id>')
@login_required
def generate_file(id):
   task_id = create_log.delay(id)
   print(task_id)
   return {'id' : str(task_id)}

@bp.route('/get_file/<id>')
def get_file(id):
   task = celery.AsyncResult(id)
   url = task.result
   return {'url' : url}
