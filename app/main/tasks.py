from app.models import User, Question
# from flask_login import current_user
import os
from app import celery

@celery.task
def create_log(id):
   current_user = User.query.get(id)
   questions = Question.query.filter_by(author=current_user)
   question_text = "Name : {}\n".format(current_user.username)
   for q in questions:
       question_text += f'''
       Question_body : {q.body}
       Question_detail: {q.detail}
       ========================================'''
   file_path = os.path.join(os.getcwd(), "app", "static", "{}.txt".format(current_user.id))
   if os.path.exists(file_path):
       os.remove(file_path)
   with open(file_path, "w") as f:
       f.write(question_text)
   url = "/static/{}.txt".format(current_user.id)
   return url