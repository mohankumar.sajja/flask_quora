from flask_wtf import FlaskForm
from wtforms.fields import StringField, TextAreaField, SubmitField, BooleanField, \
    SelectField
from wtforms.validators import ValidationError, Required, Length, Email, Regexp, DataRequired
from flask_pagedown.fields import PageDownField
from ..models import User
from flask import request

class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)

class EditProfileForm(FlaskForm):
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')

class QuestionForm(FlaskForm):
    body = StringField("What's your question?", validators=[Required()])
    detail = TextAreaField('You can add details')
    submit = SubmitField('Submit')


class AnswerForm(FlaskForm):
    body = PageDownField('Enter your answer', validators=[Required()])
    submit = SubmitField('Submit')