# Qrora Clone Using Flask

This project is code by Python + Flask

functionalities

1. ask and answer questions
2. upvote answers
3. user profiles
4. follwing others

## Setup Instructions

###VIRTUAL ENVIRONMENT
1. mkdir VirtualEnv
2. cd VirtualEnv
3. pip install flask_env
To verify a successful installation run this
    virtualenv --version

###DATABASE Migrations
After creating models do following steps
1. flask db init
2. flask db migrate -m "Models created"
3. flask db upgrade


## Installation instructions

pip install -r requirements.txt
